Build Document
==============

Requirements
--
Build a Zando-themed weather dashboard using data provided from an API.

### Specification
* API: https://api.apixu.com/v1/forecast.json?key=7d4d7ea9cc3e45ab97291728162804&q=Cape%20Town

### Minimum set of required technologies:
* Bootstrap 3
* jQuery
* Sass
* Gulp
* Git

### Documentation & Presentation
* Bitbucket Repo: https://bitbucket.org/keanan_jones/z_keanan-jones (invitation sent to leigh.pocock@zando.co.za)

### Considerations
* Caching - Weather data is only fetched every hour (the API updates every hour - on the hour and 15 mins past).
* Caching notification - upon refreshing the page a 'cached' label is displayed at the bottom right of the screen.
* Page speed - Pushing hourly data to the screen is only done on user demand.
* Fonts - Custom font was made so as to reduce over-head. Zando's fonts were also imported.

### Improvements
* In an ideal situation the server would be making the hourly API call and saving this data in cache for users.
* Weather icon png's should've been replaced with font icons, however there were over 100+ icons to match up.

Development
===========

### Production

Open the `index.html` in the `dist` folder

### Source

To run this project from source first run the following commands:

Install all npm dependencies (make sure to have [node.js](https://nodejs.org/) installed first)

```
npm install
```

Install bower dependencies

```
bower install
```

Run the project using gulp

```
gulp serve
```

~~~

