'use strict';

// 'Globals'
var oneHour = 60 * 60 * 1000;
var lastUpdated = localStorage.lastUpdated;
var getHourlyLoaded = 'no';
var weatherObj;

// Functions
// ----------------------------------------
function goToId(id) {
    $('html,body').animate({scrollTop: $('#' + id).offset().top}, 'slow');
}

function initHourlyWeather() {
    $('#hourly')[0].HTML = '';
    $.each(weatherObj.forecast.forecastday[0].hour, function (i) {
        $('#hourly')[0].innerHTML += '<div class="clearfix hourContainer">';
            $('#hourly')[0].innerHTML += '<div class="col-xs-12 col-sm-12 col-md-2 condition"><img src=\"http:' + weatherObj.forecast.forecastday[0].hour[i].condition.icon + '\">' + weatherObj.forecast.forecastday[0].hour[i].condition.text + '</div>';
            $('#hourly')[0].innerHTML += '<div class="col-xs-12 col-sm-12 col-md-1"><br/><i class=\"icon-clock\"></i> ' + weatherObj.forecast.forecastday[0].hour[i].time.substr(weatherObj.forecast.forecastday[0].hour[i].time.indexOf(' ') + 1) + '</span></div>';
            $('#hourly')[0].innerHTML += '<div class="col-xs-12 col-sm-12 col-md-1"><br/><i class=\"icon-temperature\"></i> ' + weatherObj.forecast.forecastday[0].hour[i].temp_c + '&#8451</span></div>';
            $('#hourly')[0].innerHTML += '<div class="col-xs-12 col-sm-12 col-md-2"><br/><i class=\"icon-wind\"></i>Wind: ' + weatherObj.forecast.forecastday[0].hour[i].wind_kph + ' km/h</span></div>';
            $('#hourly')[0].innerHTML += '<div class="col-xs-12 col-sm-12 col-md-2"><br/><i class=\"icon-compass\"></i>Wind direction ' + weatherObj.forecast.forecastday[0].hour[i].wind_dir + '</span></div>';
            $('#hourly')[0].innerHTML += '<div class="col-xs-12 col-sm-12 col-md-2"><br/><i class=\"icon-water\"></i>Precipitation: ' + weatherObj.forecast.forecastday[0].hour[i].precip_mm + '</span></div>';
            $('#hourly')[0].innerHTML += '<div class="col-xs-12 col-sm-12 col-md-2"><br/><i class=\"icon-fog-sun\"></i>Humidity: ' + weatherObj.forecast.forecastday[0].hour[i].humidity + '</span></div>';
        $('#hourly')[0].innerHTML += '</div>';
    });
    getHourlyLoaded = 'yes';
}

// hourly trigger
$('#hourlyTriggerBtn').click(function () {
    // check, load, show, hide
    if (getHourlyLoaded === 'no') {
        initHourlyWeather();
        goToId('hourlyContainer');
        $('#hourly').slideDown('slow');
    } else if ($('#hourly').is(':visible')) {
        $('#hourly').slideUp('slow');
    } else {
        $('#hourly').slideDown('slow');
        goToId('hourlyContainer');
    }
});

function initDailyWeather() {
    $('#daily_condition')[0].innerHTML = '<img src="http:' + weatherObj.forecast.forecastday[0].day.condition.icon + '">' + weatherObj.forecast.forecastday[0].day.condition.text;
    $('#maxtemp_c')[0].innerHTML = '<i class=\"icon-temperature maxTempIcon\"></i>Max: ' + weatherObj.forecast.forecastday[0].day.maxtemp_c + '&#8451';
    $('#mintemp_c')[0].innerHTML = '<i class=\"icon-temperature maxTempIcon\"></i>Min: ' + weatherObj.forecast.forecastday[0].day.mintemp_c + '&#8451';
    $('#avgtemp_c')[0].innerHTML = '<i class=\"icon-temperature\"></i>Avg-temp: ' + weatherObj.forecast.forecastday[0].day.avgtemp_c + '&#8451';
    $('#maxwind_kph')[0].innerHTML = '<i class=\"icon-wind\"></i>Wind: ' + weatherObj.forecast.forecastday[0].day.maxwind_kph + ' km/h';
    $('#totalprecip_mm')[0].innerHTML = '<i class=\"icon-water\"></i>Total precipitation: ' + weatherObj.forecast.forecastday[0].day.totalprecip_mm;
    $('#sunrise')[0].innerHTML = '<i class=\"icon-sunrise\"></i>Sunrise: ' + weatherObj.forecast.forecastday[0].astro.sunrise;
    $('#sunset')[0].innerHTML = '<i class=\"icon-eclipse\"></i>Sunset: ' + weatherObj.forecast.forecastday[0].astro.sunset;
}

function initCurrentWeather() {
    $('#location')[0].innerHTML = weatherObj.location.name;
    $('#setLocation').val(weatherObj.location.name.toLowerCase()).prop('selected', true);
    $('#current_condition')[0].innerHTML = '<img src="http:' + weatherObj.current.condition.icon + '">' + weatherObj.current.condition.text;
    $('#temp_c')[0].innerHTML = weatherObj.current.temp_c + '&#8451 <i class=\"icon-temperature\"></i>';
    $('#is_day')[0].innerHTML = (weatherObj.current.is_day === 0 ? 'Night' : 'Day');
    $('#current_wind_kph')[0].innerHTML = '<i class=\"icon-wind\"></i>Wind: ' + weatherObj.current.wind_kph + ' km/h';
    $('#current_wind_dir')[0].innerHTML = '<i class=\"icon-compass\"></i>Wind direction ' + weatherObj.current.wind_dir;
    $('#current_precip_mm')[0].innerHTML = '<i class=\"icon-water\"></i>Precipitation: ' + weatherObj.current.precip_mm;
    $('#current_humidity')[0].innerHTML = '<i class=\"icon-fog-sun\"></i>Humidity: ' + weatherObj.current.humidity;
    $('#feelslike_c')[0].innerHTML = '<i class=\"icon-temperature\"></i>Feels like: ' + weatherObj.current.feelslike_c + '&#8451';
}

function initWeather() {
    // retrieve local storage
    weatherObj = JSON.parse(localStorage.getItem('weatherData'));
    initCurrentWeather();
    initDailyWeather();
    getHourlyLoaded = 'no';
}

function getApiData(location) {
    $.getJSON('https://api.apixu.com/v1/forecast.json?key=7d4d7ea9cc3e45ab97291728162804&q=' + location, function (data) {
        localStorage.setItem('weatherData', JSON.stringify(data));
        localStorage.lastUpdated = new Date();
        $('#hourly').empty();
        initWeather();
    });
}

// Entry point
// ---------------------------
$(document).ready(function () {
    $('#hourly').hide();
    // reset cache if more than 1 hour has passed
    if (!lastUpdated || ((new Date() - new Date(lastUpdated)) > oneHour)) {
        getApiData('cape town');
    } else {
        $('#cached')[0].innerHTML = 'cached';
        initWeather();
    }
});
